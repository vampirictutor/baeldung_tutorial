## Spring Boot Client

This module contains articles about Spring Boot Clients

### The Course
The "REST With Spring" Classes: http://bit.ly/restwithspring

### Relevant Articles:

- [Quick Guide to @RestClientTest in Spring Boot](https://www.baeldung.com/restclienttest-in-spring-boot)
- [A Java Client for a WebSockets API](https://www.baeldung.com/websockets-api-java-spring-client)

### build&run the ws client by gradle:
```
  gradle shadowJar
  java -cp build/libs/spring-boot-client-0.0.1-SNAPSHOT-all.jar com.baeldung.websocket.client.StompClient
```

## some notes of the ws client:

1. server is located at:
    ../../spring-websockets/

2. by now, if client fails to connect to serer, it does NOT output anything.

    we may need to  tcpdump -nn -i any -X 'tcp and port 8080' :
```
      ,TS val 2841438587 ecr 2841438556], length 318: HTTP: HTTP/1.1 404
        0x0000:  4500 0172 27ac 4000 4006 13d8 7f00 0001  E..r'.@.@.......
        0x0010:  7f00 0001 1f90 bc2c 53da 6c2e ebdf 9ac8  .......,S.l.....
        0x0020:  8018 0200 ff66 0000 0101 080a a95c e97b  .....f.......\.{
        0x0030:  a95c e95c 4854 5450 2f31 2e31 2034 3034  .\.\HTTP/1.1.404
        0x0040:  200d 0a56 6172 793a 204f 7269 6769 6e0d  ...Vary:.Origin.
        0x0050:  0a56 6172 793a 2041 6363 6573 732d 436f  .Vary:.Access-Co
        0x0060:  6e74 726f 6c2d 5265 7175 6573 742d 4d65  ntrol-Request-Me
        0x0070:  7468 6f64 0d0a 5661 7279 3a20 4163 6365  thod..Vary:.Acce
        0x0080:  7373 2d43 6f6e 7472 6f6c 2d52 6571 7565  ss-Control-Reque
        0x0090:  7374 2d48 6561 6465 7273 0d0a 436f 6e74  st-Headers..Cont
        0x00a0:  656e 742d 5479 7065 3a20 6170 706c 6963  ent-Type:.applic
        0x00b0:  6174 696f 6e2f 6a73 6f6e 0d0a 5472 616e  ation/json..Tran
        0x00c0:  7366 6572 2d45 6e63 6f64 696e 673a 2063  sfer-Encoding:.c
        0x00d0:  6875 6e6b 6564 0d0a 4461 7465 3a20 5375  hunked..Date:.Su
        0x00e0:  6e2c 2031 3020 4465 6320 3230 3233 2030  n,.10.Dec.2023.0
        0x00f0:  333a 3231 3a30 3120 474d 540d 0a0d 0a36  3:21:01.GMT....6
        0x0100:  640d 0a7b 2274 696d 6573 7461 6d70 223a  d..{"timestamp":
        0x0110:  2232 3032 332d 3132 2d31 3054 3033 3a32  "2023-12-10T03:2
        0x0120:  313a 3031 2e34 3433 2b30 303a 3030 222c  1:01.443+00:00",
        0x0130:  2273 7461 7475 7322 3a34 3034 2c22 6572  "status":404,"er
        0x0140:  726f 7222 3a22 4e6f 7420 466f 756e 6422  ror":"Not.Found"
        0x0150:  2c22 7061 7468 223a 222f 7370 7269 6e67  ,"path":"/spring
```


